import React, {Fragment}  from 'react';
import * as ReactDOMClient from 'react-dom/client';

// css
import '../../css/profile.css'

import {Tabs, Tab, Container, Row, Col, OverlayTrigger, Tooltip, Nav, Card, ListGroup, Modal, Button} from 'react-bootstrap';


function Profile(){
	const pathLogos = "img/png/logos/";
	const pathLogosPaises = "img/icons/png/paises/";
	const pathIcons = "img/icons/png/";
	const pathCaratulas = "img/jpg/caratulas/";
	const pathDibujos = "img/jpg/dibujos/";
	
	const [modalShow, setModalShow] = React.useState(false);
	const [cacheDataProfile, setCacheDataProfile] = React.useState();
	
	const divTooltip = (id,tooltip) => {
		return (<Tooltip id={id}>{tooltip}</Tooltip>);
	}
	
	const addDataIntoCache = (cacheName, url, response) => {
		// Converting our response into Actual Response form
		const data = new Response(JSON.stringify(response));
		if ('caches' in window) {
			// Opening given cache and putting our data into it
			caches.open(cacheName).then((cache) => {
				cache.put(url, data);
				//alert('Data Added into cache!')
			});
		}
	};
	
	const getAllCacheData = async () => {
		var url = 'https://localhost:300'
		// List of all caches present in browser
		//var names = await caches.keys("ModalImgId");
		var name ="ModalImgId";
		var cacheDataArray = []
		// Iterating over the list of caches
		//names.forEach(async(name) => {
			// Opening that particular cache
			const cacheStorage = await caches.open(name);
			// Fetching that particular cache data
			const cachedResponse = await cacheStorage.match(url);
			var data = await cachedResponse.json()
			// Pushing fetched data into our cacheDataArray
			cacheDataArray.push(data)
			setCacheDataProfile(cacheDataArray.join(', '))
		//})
	}

	
	function LabelAttributes (props){
		return(
			<div className="input-group input-group-sm ">
				<span className="input-group-text p-0 px-1"><img src={pathIcons + props.img + ".png"} alt="item" className="icon-profile-png" /></span>
				<span className="input-group-text text-bg-dark" style={{minWidth: "55px", maxWidth:"80px", fontSize: "8px"}}>{props.attribute}</span>
				<div className="form-control text-bg-dark px-2" >{props.value}</div>
			</div>
		);
	}

	const setImgSeleted = (img) => {
		addDataIntoCache('ModalImgId','https://localhost:300',img);
		setModalShow(true);
	};
	
	const listItem = (textList) => {
		const list = [];
		list.push(<ListGroup.Item key={0} id={'treeSkillCardList'+0}>{textList[0]}</ListGroup.Item>);
		
		if(textList[1] !== undefined && textList[1].startsWith('{') && textList[1].startsWith('}',2)){
			if(textList[1].startsWith('{k}')){
				list.push(<ListGroup.Item key={'k8'} id={'treeSkillCardListk8'} style={{backgroundColor: "#FFF8"}} >8vo. kyu</ListGroup.Item>);
				list.push(<ListGroup.Item key={'k7'} id={'treeSkillCardListk7'} style={{backgroundColor: "#B2FFFF88"}} >7mo. kyu</ListGroup.Item>);
				list.push(<ListGroup.Item key={'k6'} id={'treeSkillCardListk6'} style={{backgroundColor: "#FFDF0088"}} >6to. kyu</ListGroup.Item>);
			} else if (textList[1].startsWith('{i}')){
				list.push(<ListGroup.Item key={'iES'} id={'treeSkillCardListiES'} style={{fontSize: "10px"}} >
					Español: Nativo <img src={pathLogosPaises + "7578066.png"} className="" alt="item" style={{height: "10px"}} />
					</ListGroup.Item>);
				list.push(<ListGroup.Item key={'iUS'} id={'treeSkillCardListiUS'} style={{fontSize: "10px"}} >
					Ingles: B2 <img src={pathLogosPaises + "7578061.png"} className="" alt="item" style={{height: "10px"}} />
					</ListGroup.Item>);
			}
			textList[1] = textList[1].substring(3)
		}
		
		for (var i = 1; i < textList.length; i++){
			if(textList[i]!=='')list.push(<ListGroup.Item key={i} id={'treeSkillCardList'+i}>{textList[i]}</ListGroup.Item>);
		}
		return list;
	}
	
	const setObjectInfo = (text) =>{
		document.getElementById('ObjectDescriptionText').innerHTML = text;
		document.getElementById('cardObjectInfo').style.display = '';
	}
	
	const setSkillInfo = (id, img, header, list, title, text, footer, level) => {
		if(!level) level=0;
		
		document.getElementById('cardInfoSkill').style.display = '';
		
		document.getElementById('treeSkillCardHeader').removeAttribute('class');
		document.getElementById('treeSkillCardHeader').classList.add('card-header');
		document.getElementById('treeSkillCardHeader').classList.add('fw-bold');
		document.getElementById('treeSkillCardFooter').removeAttribute('class');
		document.getElementById('treeSkillCardFooter').classList.add('card-header');
		
		const experience = [
			{levelName:"Especial", class:"bgSkillSpecial"}
			, {levelName:"Basico", class:"bgSkill"}
			, {levelName:"Intermedio", class: "bgSkillSilver"}
			, {levelName:"Avanzado", class: "bgSkillGolden"}
			, {levelName:"Experto", class: "bgSkillPlatinum"}
		];
		
		document.getElementById('treeSkillCardImg').src = pathLogos + img + ".png";
		document.getElementById('treeSkillCardHeader').innerHTML = header;
		document.getElementById('treeSkillCardHeader').classList.add(experience[level].class);
		document.getElementById('treeSkillCardTitle').innerHTML = title;
		document.getElementById('treeSkillCardText').innerHTML = text;
		document.getElementById('treeSkillCardFooter').innerHTML = footer;
		document.getElementById('treeSkillCardFooter').classList.add(experience[level].class);
		
		var containerList = document.getElementById('treeSkillGroup');
		var rootElemList = ReactDOMClient.createRoot(containerList);
		const textList = [];
		textList[0] = "Experiencia: " + experience[level].levelName;
		if (list != null){
			textList.push(...list.split("|"));
		}
		
		console.log(textList);
		rootElemList.render(listItem(textList));
	}
	
	const showPanel = () => {
		document.getElementById('panelContent').style.display = '';
	}
	
	const getListSplit = (text, separator) => {
		const list = [];
		separator = separator || ',';
		
		text = text || '';
		if(text!=='')list.push(...text.split(separator));
		return list;
	}
	
	function VgItem(props){
		var color = "#FFFF";
		var extraClass = "";
		var visibility = "";
		
		if(!props.fav)visibility = "none";
		
		switch(props.type){
			case "1":
				color = "#FFFF"
				extraClass = "text-dark"
				break;
			case "2":
				color = "#E60012"
				extraClass = "text-light"
				break;
			case "3":
				color = "#326DB3"
				extraClass = "text-light"
				break;
			case "4":
				color = "#107C0F"
				extraClass = "text-light"
				break;
			default:
				color = "#FFFF"
				extraClass = "text-dark"
				break;
		}
		
		return(
			<Col className='d-flex justify-content-center align-items-center my-1'>
				<div className='imgGame' style={{border: "5px solid "+color}}>
					<img src={pathCaratulas + props.img} className="" alt="item" style={{}} />
				</div>
				<Card style={{width: "130px", border: "2px solid " + color}}>
					<Card.Header className='ps-4 fw-bold' >{props.title}</Card.Header>
					<Card.Body className='p-1 ps-4' >
						<Card.Text style={{whiteSpace: "pre-line"}}>
							Consola: {'\n'} {props.mainConsole}
						</Card.Text>
					</Card.Body>
					<ListGroup className="list-group-flush" >
						<ListGroup.Item style={{display: visibility, backgroundColor: color}}>
							<Card.Text className={'m-0 ' + extraClass}>Favoritos:</Card.Text>
						</ListGroup.Item>
						{( () => {
							const list = getListSplit(props.fav);
							const listdiv = [];
							for (var i = 0; i < list.length; i++)listdiv.push(<ListGroup.Item key={i} >{list[i]}</ListGroup.Item>);
							return listdiv;
						})()}
					</ListGroup>
					<Card.Footer >
						<div className="p-0" style={{display: "flex",  flexWrap: "wrap"}}>
							{( () => {
								const list = getListSplit(props.tags);
								const listdiv = [];
								for (var i = 0; i < list.length; i++)listdiv.push(<div key={i} className={" tagDiv d-flex align-items-center justify-content-center " + extraClass} style={{backgroundColor: color}}>{list[i]}</div>);
								return listdiv;
							})()}
						</div>
					</Card.Footer>
				</Card>
			</Col>
		);
	}
	
	function ImgItem(props){
		return(
			<Col className={'imgContainer ' + props.orientation} onClick={() => setImgSeleted(props.img)}>
				<img id={props.id} src={pathDibujos + props.img} className={""} alt="item" style={{}} />
			</Col>
		);
	}
	
	function SerieItem(props){
		var imgPais = "";
		var color = "";
		
		if(props.type.includes("Anime") || props.type.includes("Manga")){
			color = "#FFFF";
			imgPais = "899396.png";
		} else if(props.type.includes("Manhwa")){
			color = "#FFFF";
			imgPais = "899378.png";
		} else if(props.type.includes("Manhua") || props.type.includes("Donghua")){
			color = "#FFFF";
			imgPais = "833864.png";
		} else {
			color = "#FFFF";
			imgPais = "";
		}
		
		return(
			<Col className='d-flex  '>
				<div className=' flip-card '>
				<div className=' flip-card-inner d-flex'>
					<Card className='flip-card-front ' style={{}}>
						<div className='gradoCarta' style={{}}>{props.grado}</div>
						<Card.Img variant="top" src={pathCaratulas + "m-"+props.img+"-f.jpg"} />
						<div className='footerCarta' style={{}}>{props.personaje}</div>
					</Card>
					<Card className='flip-card-back ' style={{}}>
						<Card.Header className='fw-bold' >{props.title}</Card.Header>
						<Card.Img variant="top" src={pathCaratulas + "m-"+props.img+"-b.jpg"} />
						<Card.Body className='' >
						</Card.Body>
						<ListGroup className="list-group-flush" >
							<ListGroup.Item style={{display: "", backgroundColor: color}}>
								Autor: <br /> {props.autor}
							</ListGroup.Item>
							<ListGroup.Item style={{fontSize: "10px"}}>Estado: {props.estado}</ListGroup.Item>
							<ListGroup.Item style={{fontSize: "10px"}}>
								Tipo: <br /> {props.type}
								<img src={pathLogosPaises + imgPais} className="iconPaises" alt="item" style={{}} />
							</ListGroup.Item>
						</ListGroup>
						<Card.Footer >
							<div className="p-0" style={{display: "flex",  flexWrap: "wrap"}}>
								{( () => {
									const list = getListSplit(props.tags);
									const listdiv = [];
									for (var i = 0; i < list.length; i++)listdiv.push(<div key={i} className={" tagDivSerie d-flex align-items-center justify-content-center "} style={{backgroundColor: color}}>{list[i]}</div>);
									return listdiv;
								})()}
							</div>
						</Card.Footer>
					</Card>
				</div>
				</div>
			</Col>
		);	
	}
	
	function SongItem(props){
		
		var color = "#FFFF"
		
		return(
			<Col sm={6} className=' '>
				<div className='song' style={{}}>
					<Card className='album' style={{width: "148px", borderRadius: "0px" }}>
						<Card.Header className='p-0 d-flex align-items-center justify-content-center bgSkillGolden fw-bold' style={{height: "40px"}}>{props.title}</Card.Header>
						<ListGroup className="list-group-flush" >
							<ListGroup.Item className='' style={{backgroundColor: "#FFF", height: "50px"}}>
								<Card.Text className={' '} style={{fontSize: "10px"}} >Autor: <br />{props.autor}</Card.Text>
							</ListGroup.Item>
						</ListGroup>
						<Card.Footer className='py-1'>
							<div className="p-0" style={{display: "flex",  flexWrap: "wrap", height: "50px"}}>
								{( () => {
									const list = getListSplit(props.tags);
									const listdiv = [];
									for (var i = 0; i < list.length; i++)listdiv.push(<div key={i} className={" tagDivSerie d-flex align-items-center justify-content-center "} style={{backgroundColor: color}}>{list[i]}</div>);
									return listdiv;
								})()}
							</div>
						</Card.Footer>
					</Card>
					<div className='disco' style={{}} >
						<img src={pathCaratulas + props.img } className="" alt="item" style={{}} />
					</div>
				</div>
			</Col>
		);
	}
	
	function ModalImg(props){
		getAllCacheData();
		console.log(cacheDataProfile);
		return(
			<Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-vcenter">
						Dibujo ampliado
					</Modal.Title>
				</Modal.Header>
				<Modal.Body className='d-flex justify-content-center '>
					<img id={"imgModal"} src={pathDibujos + cacheDataProfile} className={""} alt="item" style={{width: "80%"}} />
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={props.onHide}>Cerrar</Button>
				</Modal.Footer>
			</Modal>
		);
	}
	
	return (
		<Fragment>
			<Row className='bgPerfil p-2 pb-4 mb-4'>
				<Col sm={12} className='d-flex justify-content-center align-items-center' style={{}}>
					<h1 className='my-4'>My Perfil</h1>
				</Col>
				<Col sm={4} className='profile-card-info' style={{minHeight: "600px"}}>
					<Row style={{
						boxShadow: '#0001 0px 2px, #0001 0px 4px, #0001 0px 6px, #0001 0px 8px, #0002 0px 10px'
						, background: '#0004'
						, minHeight: "20px"
						, border: "1px solid #0008"
						}}>
						{/* Header card */}
						<Col className='py-2 text-center' style={{paddingLeft: "0px"}}>
							<div className='text-bg-dark py-1' style={{border: "1px solid #FFF6", borderLeft: "0px solid #0000"}}>
								<img src="img/icons/png/10427338.png" className='float-start' alt="item" style={{width:"20px", transform: "scaleX(-1)", filter: "invert(100%)"}}/>
								<div>Steven</div>
							</div>
						</Col>
						<Col className='' >
							<div className="progress bg-dark mt-2" style={{height: "10px", borderRadius:"0px"}}>
								<div className="progress-bar bgSkillSpecial text-light fw-bold" style={{width: "90%"}} aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">LIFE</div>
							</div>
							<div className="progress bg-dark my-1" style={{height: "5px", borderRadius:"0px"}}>
								<div className="progress-bar bg-light text-dark fw-bold" style={{width: "75%"}} aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">ENERGY</div>
							</div>
							<div className="progress bg-dark my-1" style={{height: "5px", borderRadius:"0px"}}>
								<div className="progress-bar bgSkillGolden text-dark fw-bold" style={{width: "20%"}} aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">WILL</div>
							</div>
						</Col>
					</Row>
					<Row >
						<Col className='' >
							{/* Tabs navs */}
							<Tabs defaultActiveKey="estatus" id="ex1" className="my-3"  justify >
								<Tab eventKey="estatus" 
									title={<div><img src="img/icons/png/2611008.png" alt="item" className="icon-profile-png-title" />Estatus</div>} 
									tabClassName="text-light btnTabFav"
									style={{backgroundColor: "#0000", borderRadius: "0px"}}>
									{/*Tabs content*/}
									<Row>
										<Col>
											<img src="img/png/profile.png" className='mb-5 mt-4' alt="Lights" style={{width:"100%", transform: "scaleX(1)", maxHeight: "280px"}}/>
											<div className="input-group input-group-sm">
												<span className="input-group-text bgSkillGolden text-dark" style={{minWidth: "75px", maxWidth:"80px"}}>Nivel</span>
												<div className="form-control ps-3" >25</div>
											</div>
											<div className="input-group input-group-sm my-1" >
												<span className="input-group-text bgSkillGolden text-dark" style={{minWidth: "75px", maxWidth:"80px"}}>Nacionalidad</span>
												<div className="form-control py-0 ps-3" >
													<OverlayTrigger placement="right" overlay={divTooltip('nacionalidad','Colombiano')}>
														<img src={pathLogosPaises + "7578024.png"} className="" alt="item" style={{height: "25px"}} />
													</OverlayTrigger>
												</div>
											</div>
											<div className="input-group input-group-sm" >
												<span className="input-group-text bg-dark text-light" style={{minWidth: "75px", maxWidth:"80px"}}>Altura</span>
												<div className="form-control ps-3" >1.66 m</div>
											</div>
											<div className="input-group input-group-sm" >
												<span className="input-group-text bg-dark text-light" style={{minWidth: "75px", maxWidth:"80px"}}>Peso</span>
												<div className="form-control ps-3" >70 Kg</div>
											</div>
											<div className="input-group input-group-sm my-1">
												<span className="input-group-text bgSkillSpecial text-light" style={{minWidth: "75px", maxWidth:"80px"}}>Idiomas</span>
												<div className="form-control py-0 ps-3" >
													<OverlayTrigger placement="right" overlay={divTooltip('idioma-1','Español')}>
														<img src={pathLogosPaises + "7578066.png"} className="" alt="item" style={{height: "25px"}} />
													</OverlayTrigger>
													<OverlayTrigger placement="right" overlay={divTooltip('idioma-2','Ingles')}>
														<img src={pathLogosPaises + "7578061.png"} className="" alt="item" style={{height: "25px"}} />
													</OverlayTrigger>
												</div>
											</div>
										</Col>
										<Col>
											<LabelAttributes img="fuerza" attribute="Fuerza" value="6" />
											<LabelAttributes img="9978037" attribute="Destreza" value="8" />
											<LabelAttributes img="vitalidad" attribute="Vitalidad" value="7" />
											<LabelAttributes img="4406307" attribute="Agilidad" value="5" />
											<LabelAttributes img="9826440" attribute="Inteligencia" value="8" />
											<LabelAttributes img="568149" attribute="Percepción" value="8" />
											<LabelAttributes img="6550988" attribute="Espiritud" value="2" />
											<LabelAttributes img="9348487" attribute="Suerte" value="1" />
										</Col>
									</Row>
								</Tab>
								<Tab eventKey="detalle" 
									title={<div><img src="img/icons/png/3723653.png" alt="item" className="icon-profile-png-title" />Detalle</div>} 
									tabClassName="text-light btnTabFav" 
									style={{backgroundColor: "#0000", borderRadius: "0px"}}>
									{/*Tabs content*/}
									<Row>
										<Col className="p-2">
											<h4 className="mt-0">Nombre completo</h4>
											<div className="input-group input-group-sm">
												<div className="form-control py-0 text-bg-dark" >Pedro Steven Castiblanco Piracoca</div>
											</div>
											<h4 className="mt-3">Profesion</h4>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Programador </div>
												<span className="input-group-text py-0">Lv. 3</span>
											</div>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Analista en soporte</div>
												<span className="input-group-text py-0">Lv. 3</span>
											</div>
											<h4 className="mt-3">Titulos</h4>
											<div className="input-group input-group-sm">
												<span className="input-group-text bgSkillGolden"><img src="img/icons/png/3723653.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0 " >Técnico Profesional en Computación</div>
											</div>
											<div className="input-group input-group-sm">
												<span className="input-group-text bgSkillGolden"><img src="img/icons/png/3723653.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0 " >Tecnólogo en Desarrollo de Software</div>
											</div>
											<h4 className="mt-3">Habilidades duras (Activas)</h4>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Programar</div>
												<span className="input-group-text py-0">Lv. 4</span>
											</div>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Dibujar</div>
												<span className="input-group-text py-0">Lv. 4</span>
											</div>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Analizar </div>
												<span className="input-group-text py-0">Lv. 3</span>
											</div>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Crear manualidades </div>
												<span className="input-group-text py-0">Lv. 4</span>
											</div>
											<h4 className="mt-3">Habilidades blandas (Pasivas)</h4>
											<div className="input-group input-group-sm">
												<span className="input-group-text"><img src="img/icons/png/4406328.png" alt="item" className="icon-profile-png" /></span>
												<div className="form-control py-0" >Adaptabilidad </div>
												<span className="input-group-text py-0">Lv. 4</span>
											</div>
										</Col>
									</Row>
								</Tab>
								<Tab eventKey="objetos" 
									title={<div><img src="img/icons/png/4406520.png" alt="item" className="icon-profile-png-title" />Objetos</div>} 
									tabClassName="text-light btnTabFav" 
									style={{backgroundColor: "#0000", borderRadius: "0px"}}>
									{/*Tabs content*/}
									<h3>Equipamiento</h3>
									<Row className="mt-5">
										<Col></Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-1','Collar')}>
												<div className='bgSkillGolden p-3  mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Objeto representativo de un legendario guerrero.')} >
													<img src="img/icons/png/4414836.png" className=" mx-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-2','Bufanda')}>
											<div className='bg-light p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Bufanda normal personalizada.')} >
												<img src="img/icons/png/5799203.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
											</div>
											</OverlayTrigger>
										</Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-3','Relicario')}>
												<div className='bgSkillGolden p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Tesoro que guarda valiosos recuerdos.')} >
													<img src="img/icons/png/8430355.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col></Col>
									</Row>
									<Row className="mt-0">
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-4','Brazalete Runico')}>
												<div className='bgSkillGolden p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Brazalete de antebrazo con runas de protección.')} >
													<img src="img/icons/png/10407848.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-7','Pulsera Tribal')}>
												<div className='bg-light p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Objeto tribal con un valioso recuerdo.')} >
													<img src="img/icons/png/3007315.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-5','Manilla')}>
												<div className='bg-light p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Objeto normal con una caracteristica especial.')} >
													<img src="img/icons/png/4406455.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-6','Pulsera USB')}>
												<div className='bgSkillGolden p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Pulsera con la capacidad de guardar datos.')} >
													<img src="img/icons/png/7267502.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
									</Row>
									<Row className="mt-0">
										<Col></Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-8','Canillera')}>
												<div className='bg-light p-3  mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Articulo de protección, otorga +5 de defensa.')} >
													<img src="img/icons/png/9967790.png" className=" mx-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col className='text-center'>
											<div className='p-3 m-0' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)"}}>
											</div>
										</Col>
										<Col className='text-center'>
											<OverlayTrigger placement="top" overlay={divTooltip('tooltip-8','Piernera Táctica')}>
												<div className='bg-light p-3 mx-auto d-block' style={{width: "60px", clipPath: "polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)", cursor: "pointer"}}
													onClick={() => setObjectInfo('Objeto con capacidad de otorgar inventario adicional.')} >
													<img src="img/icons/png/10407856.png" className="my-auto" alt="item" style={{width: "30px", height: "30px"}} />
												</div>
											</OverlayTrigger>
										</Col>
										<Col></Col>
									</Row>
									<Row id='cardObjectInfo' className="mx-3 my-4" style={{display: "none"}}>
										<Card className='px-0'>
											<Card.Header className='fw-bold' >Descripción</Card.Header>
											<Card.Body id='ObjectDescription' className='' >
												<Card.Text id='ObjectDescriptionText' className=' mb-0 ' style={{}} ></Card.Text>
											</Card.Body>
										</Card>
									</Row>
								</Tab>
							</Tabs>
							{/* Tabs navs */}
						</Col>
					</Row>
				</Col>
				<Col sm={8} className=''>
					{/* Tab favoritos */}
					<Tab.Container id="left-tabs-example" defaultActiveKey=""> {/*defaultActiveKey="first"*/}
						<Row style={{ minHeight: "500px"}} className=''>
							<Col sm={2} className="px-2" style={{backgroundColor: "#0000", margin: "50px 0px" }}>
								<Nav variant="pills" id="menuPanel" className="flex-column " style={{backgroundColor: "#0000" }} >
									{/* Menu favoritos */}
									<Nav.Item style={{}} >
										<Nav.Link eventKey="first" className='d-flex justify-content-center align-items-center btnFav' style={{minHeight: "100px"}} onClick={() => showPanel()} >
											<img src="img/icons/png/9348520.png" className="imgIconFav" alt="item" style={{}} />
										</Nav.Link>
									</Nav.Item>
									<Nav.Item>
										<Nav.Link eventKey="second" className='d-flex justify-content-center align-items-center btnFav' style={{height: "100px"}} onClick={() => showPanel()} >
											<img src="img/icons/png/3346055.png" className="imgIconFav" alt="item" style={{}} />
										</Nav.Link>
									</Nav.Item>
									<Nav.Item>
										<Nav.Link eventKey="third" className='d-flex justify-content-center align-items-center btnFav' style={{height: "100px"}} onClick={() => showPanel()} >
											<img src="img/icons/png/1562171.png" className="imgIconFav" alt="item" style={{}} />
										</Nav.Link>
									</Nav.Item>
									<Nav.Item>
										<Nav.Link eventKey="fourth" className='d-flex justify-content-center align-items-center btnFav' style={{height: "100px"}} onClick={() => showPanel()} >
											<img src="img/icons/png/4322586.png" className="imgIconFav" alt="item" style={{}} />
										</Nav.Link>
									</Nav.Item>
									<Nav.Item>
										<Nav.Link eventKey="fifth" className='d-flex justify-content-center align-items-center btnFav' style={{height: "100px"}} onClick={() => showPanel()} >
											<img src="img/icons/png/7509875.png" className="imgIconFav" alt="item" style={{}} />
										</Nav.Link>
									</Nav.Item>
								</Nav>
							</Col>
							<Col sm={10} className="p-0" >
								<Tab.Content className='profile-card-item p-0' id="panelContent" style={{ minHeight: "600px", display: "none"}} >
									<Tab.Pane eventKey="first" >
										{/* Tab First */}
										<Container>
											<Row className="">
												<Col  className='d-flex justify-content-center align-items-center my-3'>
													<h3>Arbol de habilidades</h3>
												</Col>
											</Row>
											<Row className="mt-3">
												<Col sm={9} className="align-bottom" style={{display: 'inline-block', verticalAlign: 'middle'}} >
													<Row>
														<Col sm={8}>
															{/* Arbol de programacion */}
															<Row>
																<Col>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('backend-5','React')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkill' style={{}}
																			onClick={() => setSkillInfo('2','9891417','React', null,'React','Experiencia: Intermedio','',1)} >
																			<img src={pathLogos + "9891417.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('database-4','MySql')}>
																		<div className='p-2 mx-auto d-block iconBgSkillSilver baseIconSkill' style={{}} 
																			onClick={() => setSkillInfo('1','4691303','MySql',null,'MySql','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "4691303.png"} className="iconSkill" alt="item"  />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('backend-4','Angular')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkill' style={{}}
																			onClick={() => setSkillInfo('2','4691504','Angular', null,'Angular','Experiencia: Intermedio','',1)} >
																			<img src={pathLogos + "4691504.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('frontend-4','TypeScript')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkill' style={{}}
																			onClick={() => setSkillInfo('0','9846292','TypeScript',null,'TypeScript','Experiencia: Intermedio','',1)} >
																			<img src={pathLogos + "9846292.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('database-3','PostgreSql')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}} 
																			onClick={() => setSkillInfo('1','5838697','PostgreSql',null,'PostgreSql','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "5838697.png"} className="iconSkill" alt="item"  />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('backend-3','C++')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}}
																			onClick={() => setSkillInfo('2','1708381','C++',null,'C++','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "1708381.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('frontend-3','JavaScript')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}}
																			onClick={() => setSkillInfo('1','9846157','JavaScript',null,'JavaScript','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "9846157.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('database-2','SqlServer')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}} 
																			onClick={() => setSkillInfo('1','SQL-Server','Oracle',null,'Oracle','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "SQL-Server.png"} className="iconSkill" alt="item"  />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('backend-2','C#')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}}
																			onClick={() => setSkillInfo('2','9845133','C#',null,'C#','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "9845133.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('frontend-2','CSS3')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillGolden' style={{}}
																			onClick={() => setSkillInfo('2','7945115','CSS3',null,'CSS3','Experiencia: Intermedio','',3)} >
																			<img src={pathLogos + "7945115.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('database-1','Oracle')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}} 
																			onClick={() => setSkillInfo('1','logo-oracle','Oracle',null,'Oracle','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "logo-oracle.png"} className="iconSkill" alt="item"  />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('backend-1','Java')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{}} 
																			onClick={() => setSkillInfo('1','5163668','Java',null,'Java','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "5163668.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('frontend-1','Html')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillGolden' style={{}}
																			onClick={() => setSkillInfo('2','7945043','Html',null,'Html','Experiencia: Intermedio','',3)} >
																			<img src={pathLogos + "7945043.png"} className="iconSkill" alt="item" />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row className='my-1'>
																<Col className='text-center'>
																	<h6>Database</h6>
																</Col>
																<Col className='text-center'>
																	<h6>Backend</h6>
																</Col>
																<Col className='text-center'>
																	<h6>Frontend</h6>
																</Col>
															</Row>
														</Col>
														<Col sm={4}>
															<Row className='m-4'>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-7','Photoshop')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','4865757','Adobe Photoshop',null,'Photoshop','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "4865757.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-8','VEGAS Pro')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','sony-vegas-black','Sony VEGAS Pro',null,'VEGAS Pro','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "sony-vegas-black.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-5','GitHub')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkill' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','568392','GitHub',null,'GitHub','Experiencia: Intermedio','',1)} >
																			<img src={pathLogos + "568392.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-6','Unity')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','5163800','Unity',null,'Unity','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "5163800.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-3','Visual Studio')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','7944956','Visual Studio', null,'Visual Studio','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "7944956.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-4','Visual Studio Code')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','7938470','Visual Studio Code',null,'Visual Studio Code','Experiencia: Intermedio','',2)} >
																			<img src={pathLogos + "7938470.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
															<Row>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-1','Eclipse')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillGolden' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','218719-1','Eclipse IDE',null,'Eclipse','Experiencia: Intermedio','',3)} >
																			<img src={pathLogos + "218719-1.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
																<Col>
																	<OverlayTrigger placement="top" overlay={divTooltip('programs-2','Netbeans')}>
																		<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillGolden' style={{width: "45px"}}
																			onClick={() => setSkillInfo('2','9853026','Netbeans IDE',null,'Netbeans','Experiencia: Intermedio','',3)} >
																			<img src={pathLogos + "9853026.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																		</div>
																	</OverlayTrigger>
																</Col>
															</Row>
														</Col>
													</Row>
													<Row className='my-2'>
														<Col sm={8} className='text-center'>
															<h4>Programación</h4>
														</Col>
														<Col sm={4} className='text-center'>
															<h4>Programas</h4>
														</Col>
													</Row>
													<Row className='my-4'>
														<Col sm={2} className='text-center'>
															<h4>Especial</h4>
														</Col>
														<Col >
															<OverlayTrigger placement="top" overlay={divTooltip('special-1','Karate')}>
																<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSpecial' style={{width: "45px"}}
																	onClick={() => setSkillInfo('2','941901','Karate','{k}','Karate Seishokan','Experiencia: 3 años','',0)} >
																	<img src={pathLogos + "941901.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																</div>
															</OverlayTrigger>
														</Col>
														<Col >
															<OverlayTrigger placement="top" overlay={divTooltip('special-2','Dibujo')}>
																<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSpecial' style={{width: "45px"}}
																	onClick={() => setSkillInfo('2','9881624','Dibujo','Lapiz|Pintura','Dibujo','Nivel 6 <br> Manejo del color y sombras','',0)} >
																	<img src={pathLogos + "9881624.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																</div>
															</OverlayTrigger>
														</Col>
														<Col >
															<OverlayTrigger placement="top" overlay={divTooltip('special-3','Manualidades')}>
																<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSpecial' style={{width: "45px"}}
																	onClick={() => setSkillInfo('2','7374303','Manualidades','Carpinteria Nv. Basico|Hama beans Nv. Intermedio','Manualidades','Habilidad de materializar lo planificado.','',0)} >
																	<img src={pathLogos + "7374303.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																</div>
															</OverlayTrigger>
														</Col>
														<Col>
															<OverlayTrigger placement="top" overlay={divTooltip('special-4','Piano')}>
																<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSilver' style={{width: "45px"}}
																	onClick={() => setSkillInfo('2','9828415','Piano', 'Lectura de pentagramas Nv. Intermedio','Piano','Experiencia: Intermedio','',2)} >
																	<img src={pathLogos + "9828415.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																</div>
															</OverlayTrigger>
														</Col>
														<Col >
															<OverlayTrigger placement="top" overlay={divTooltip('special-5','Idiomas')}>
																<div className='p-2 mx-auto d-block baseIconSkill iconBgSkillSpecial' style={{width: "45px"}}
																	onClick={() => setSkillInfo('2','6125464','Idiomas','{i}','Idiomas','Bilingüe','',0)} >
																	<img src={pathLogos + "6125464.png"} className="" alt="item" style={{width: "30px", height: "30px"}} />
																</div>
															</OverlayTrigger>
														</Col>
													</Row>
												</Col>
												<Col sm={3}>
													<Card className="text-center" id='cardInfoSkill' style={{display: "none"}}>
														<Card.Header id='treeSkillCardHeader' className='fw-bold'>Habilidad</Card.Header>
														<Card.Img id='treeSkillCardImg' variant="top" style={{height: "100px"}} />
														<ListGroup className="list-group-flush" id='treeSkillGroup'>
														</ListGroup>
														<Card.Body>
															<Card.Title id='treeSkillCardTitle'>Habilidad</Card.Title>
															<Card.Text id='treeSkillCardText'>
																Contenido
															</Card.Text>
														</Card.Body>
														<Card.Footer id='treeSkillCardFooter' className="text-muted"></Card.Footer>
													</Card>
												</Col>
											</Row>
										</Container>
									</Tab.Pane>
									<Tab.Pane eventKey="second" className=''>
										{/* Tab Second */}
										<Container className='' >
											{/* Tabs Dibujos */}
											<Row className="">
												<Col className='d-flex justify-content-center align-items-center my-3'>
													<h3>Dibujos</h3>
												</Col>
											</Row>
										</Container>
										<Container style={{overflowY: "scroll", height: "545px"}}>
											<Row className="drawGallery">
												<input type="hidden" id="valueHidden" value=""/>
												<ImgItem id="d-1" img="d-1.jpg" orientation="h" />
												<ImgItem id="d-2" img="d-2.jpg" orientation="h" />
												<ImgItem id="d-3" img="d-3.jpg" orientation="h" />
												<ImgItem id="d-4" img="d-4.jpg" orientation="h" />
												<ImgItem id="d-5" img="d-5.jpg" orientation="v" />
												<ImgItem id="d-6" img="d-6.jpg" orientation="v" />
												<ImgItem id="d-7" img="d-7.jpg" orientation="v" />
												<ImgItem id="d-8" img="d-8.jpg" orientation="v" />
												<ImgItem id="d-9" img="d-9.jpg" orientation="h" />
												<ImgItem id="d-10" img="d-10.jpg" orientation="h" />
												<ImgItem id="d-11" img="d-11.jpg" orientation="h" />
												<ImgItem id="d-12" img="d-12.jpg" orientation="h" />
												<ImgItem id="d-13" img="d-13.jpg" orientation="v" />
												<ImgItem id="d-14" img="d-14.jpg" orientation="v" />
												<ImgItem id="d-15" img="d-15.jpg" orientation="v" />
												<ImgItem id="d-16" img="d-16.jpg" orientation="v" />
											</Row>
											<ModalImg show={modalShow} onHide={() => setModalShow(false)} />
										</Container>
									</Tab.Pane>
									<Tab.Pane eventKey="third" className=''>
										{/* Tab Third */}
										<Container className='' >
											{/* Tabs Videojuegos */}
											<Row className="">
												<Col className='d-flex justify-content-center align-items-center my-3'>
													<h3>Videojuegos favoritos</h3>
												</Col>
											</Row>
										</Container>
										<Container style={{overflowY: "scroll", height: "545px"}}>
											<Row className='' >
												<Col sm={1} className='d-flex justify-content-center align-items-center' style={{backgroundColor: "#FFF"}}>
													<img src={pathLogos + "7945020.png"} className="" alt="item" style={{width: "50px", height: "50px"}} />
												</Col>
												<Col className='py-2' style={{backgroundColor: "#0002", border: "4px solid #FFF"}}>
													<Row className="">
														<VgItem img="vg-beginners.jpg" title="The beginner's guide" type="1" mainConsole="PC" fav="" tags="Indie,Interactive storytelling,Aventura"/>
														<VgItem img="vg-gris.jpg" title="Gris" type="1" mainConsole="PC" fav="" tags="Indie,Plataformas,Aventura,Puzzle"/>
														<VgItem img="vg-superliminal.jpg" title="Superliminal" type="1" mainConsole="PC" fav="" tags="Aventura grafica,Puzzle"/>
														<VgItem img="vg-slay.jpg" title="Slay the Spire" type="1" mainConsole="PC" fav="" tags="Indie,Roguelike,Fantasia,Estrategia"/>
														<VgItem img="vg-loopHero.jpg" title="Loop hero" type="1" mainConsole="PC" fav="" tags="Indie,Roguelike,Fantasia oscura,Rol,Táctico"/>
														<VgItem img="vg-thisWarOfMine.jpg" title="This war of mine" type="1" mainConsole="PC" fav="" tags="Indie,Posapocalítico,Gestión,Bélico,Supervivencia"/>
													</Row>
												</Col>
											</Row>
											<Row className="" >
												<Col sm={1} className='d-flex justify-content-center align-items-center p-2' style={{backgroundColor: "#E60012"}}>
													<img src={pathLogos + "7944949.png"} className="" alt="item" style={{width: "50px", height: "50px"}} />
												</Col>
												<Col className='py-2' style={{backgroundColor: "#0002", border: "4px solid #E60012"}}>
													<Row className="">
														<VgItem img="vg-tloz.jpg" title="The legend of zelda" type="2" mainConsole="Nintendo Switch" fav="breath of the wild" tags="Mundo abierto,Rol,Acción,Avetura" />
														<VgItem img="vg-octopath.jpg" title="Octopath Traveler" type="2" mainConsole="Nintendo Switch" fav="" tags="Mundo abierto,Rol,Anime,Avetura" />
														<VgItem img="vg-marioKart.jpg" title="Mario Kart 8 Deluxe" type="2" mainConsole="Nintendo Switch" fav="" tags="Conduccion" />
														<VgItem img="vg-dqb2.jpg" title="Dragon quest builders 2" type="2" mainConsole="Nintendo Switch" fav="" tags="Sandbox,Rol,Construcción,Aventura" />
													</Row>
												</Col>
											</Row>
											<Row className="">
												<Col sm={1} className='d-flex justify-content-center align-items-center p-2' style={{backgroundColor: "#326DB3"}}>
													<img src={pathLogos + "7944968.png"} className="" alt="item" style={{width: "50px", height: "50px"}} />
												</Col>
												<Col className='py-2' style={{backgroundColor: "#0002", border: "5px solid #326DB3"}}>
													<Row className="">
														<VgItem img="vg-crash1.jpg" title="Saga Crash Bandicoot" type="3" mainConsole="Play Station 1" 
															fav="Crash Bandicoot,Crash Bandicoot 2,Crash Bandicoot 3" tags="Plataformas,Acción,Aventura" />
														<VgItem img="vg-crashBash.jpg" title="Crash Bash" type="3" mainConsole="Play Station 1" 
															fav="" tags="Minijuegos,Acción,Party" />
														<VgItem img="vg-gowRagnarok.jpg" title="Saga God Of War" type="3" mainConsole="Play Station 2, Play Station 3, Play Station 4, Play Station 5" 
															fav="God Of War,God Of War 2, God Of War 3, God Of War 2018, God Of War: Ragnarok" tags="Hack and Slash,Rol,Acción,Aventura" />
														<VgItem img="vg-dmc5.jpg" title="Saga Devil May Cry" mainConsole="Play Station 2, Play Station 3,Play Station 4" 
															fav="Devil May Cry 3,Devil May Cry 4,Devil May Cry 5" type="3" tags="Hack and Slash,Rol,Acción,Aventura,Fantasia" />
													</Row>
												</Col>
											</Row>
											<Row className="">
												<Col sm={1} className='d-flex justify-content-center align-items-center p-2' style={{backgroundColor: "#107C0F"}}>
													<img src={pathLogos + "7944980.png"} className="" alt="item" style={{width: "50px", height: "50px"}} />
												</Col>
												<Col className='py-2' style={{backgroundColor: "#0002", border: "5px solid #107C0F"}}>
													<Row className="">
														<VgItem img="vg-halo2.jpg" title="Saga Halo" mainConsole="Xbox Classic" fav="Halo Combat Evolved, Halo 2" type="4" tags="Shooter,Aventura" />
													</Row>
												</Col>
											</Row>
										</Container>
									</Tab.Pane>
									<Tab.Pane eventKey="fourth">
										{/* Tab Fourth */}
										<Container className='' >
											{/* Tabs Animes */}
											<Row className="">
												<Col className='d-flex justify-content-center align-items-center my-3'>
													<h3>Series favoritas</h3>
												</Col>
											</Row>
										</Container>
										<Container style={{overflowY: "scroll", height: "545px"}}>
											<Row className='' >
												<Col className='py-3 ' style={{backgroundColor: "#0008", border: "5px solid #FFD700"}}>
													<Row className="">
														<SerieItem img="db" title="Dragon ball" autor="Akira Toriyama" estado="En emisión" type="Manga,Anime" 
															tags="Shounen,Acción,Artes marciales,Aventuras,Superpoderes" grado="Z" personaje="Son Goku" />
														<SerieItem img="naruto" title="Naruto" autor="Masashi Kishimoto" estado="En emisión" type="Manga,Anime" 
															tags="Shounen,Acción,Artes marciales,Superpoderes" grado="SS" personaje="Uzumaki Naruto"/>
														<SerieItem img="khr" title="Katekyo Hitman Reborn" autor="Akira Amano" estado="Finalizada" type="Manga,Anime" 
															tags="Shounen,Acción,Superpoderes" grado="S" personaje="Sawada Tsunayoshi"/>
														<SerieItem img="dgm" title="D.Gray-man" autor="Katsura Hoshino" estado="En emisión" type="Manga,Anime" 
															tags="Shounen,Acción,Artes marciales,Superpoderes" grado="S" personaje="Walker Allen"/>
														<SerieItem img="sno" title="Sora no otoshimono" autor="Suu Minazuki" estado="Finalizada" type="Manga,Anime" 
															tags="Shounen,Ciencia ficción,Romance,Ecchi,Harem" grado="S" personaje="Ikarus"/>
														<SerieItem img="tlr" title="To Love-Ru" autor="Saki Hasemi" estado="Finalizada" type="Manga,Anime" 
															tags="Ciencia ficción,Romance,Ecchi,Harem,Escolar" grado="S" personaje="Satalin Deviluke Lala"/>
														<SerieItem img="opm" title="One punch man" autor="One" estado="En emisión" type="Manga,Anime" 
															tags="Acción,Ciencia ficcion,Superpoderes,Parodia" grado="Z" personaje="Saitama"/>
														<SerieItem img="mp100" title="Mob Psycho 100" autor="One" estado="Finalizada" type="Manga,Anime" 
															tags="Acción,Comedia,Sobrenatural" grado="SS" personaje="Kageyama Shigeo (Mob)"/>

														<SerieItem img="tgohs" title="The God of High School" autor="Yongje Park" estado="Finalizada" type="Manhwa" 
															tags="Acción,Artes marciales,Comedia" grado="Z" personaje="Mori Jin"/>
														<SerieItem img="soloL" title="Solo Leveling" autor="Chugong" estado="Finalizada" type="Manhwa" 
															tags="Acción,Aventuras,Fantasia,Sobrenatural" grado="Z" personaje="Sung Jin-Woo"/>
														<SerieItem img="armsbs" title="A returner's magic should be special" autor="Yook So-Nan" estado="En emisión" type="Manhwa" 
															tags="Acción,Reencarnación,Apocalíptico,Fantasia,Drama,Magia" grado="S" personaje="Arman Desir"/>
														<SerieItem img="tbate" title="The beginning after the end" autor="TurtleMe" estado="En emisión" type="Manhwa" 
															tags="Acción,Reencarnación,Aventuras,Fantasia,Magia,Superpoderes" grado="S" personaje="Leywin Arthur"/>
														<SerieItem img="studyG" title="Study Group" autor="Sin Hyeong Wook" estado="En emisión" type="Manhwa" 
															tags="Acción,Artes marciales,Romance,Escolar" grado="A+" personaje="Yoon Gamin"/>
														<SerieItem img="lookism" title="Lookism" autor="Park Tae-Jun" estado="En emisión" type="Manhwa" 
															tags="Acción,Artes marciales,Romance,Comedia,Drama,Escolar" grado="A+" personaje="Park Daniel"/>
															
														<SerieItem img="combatC" title="Combat Continent" autor="Tang Jia San Shao" estado="En emisión" type="Manhua,Donghua" 
															tags="Acción,Cultivación,Aventuras,Artes marciales,Fantasia,Sobrenatural,Romance,Escolar" grado="SS" personaje="Tang San"/>
														<SerieItem img="todag" title="Tales of Demons and Gods" autor="Mad Snail" estado="En emisión" type="Manhua,Donghua" 
															tags="Acción,Cultivación,Reencarnación,Aventuras,Artes marciales,Fantasia,Romance" grado="SS" personaje="Nie Li"/>
														<SerieItem img="hiqalta" title="Hero? I quit a long time ago" autor="Bored to See the Day" estado="En emisión" type="Manhua,Donghua" 
															tags="Acción,Artes marciales,Sobrenatural,Superpoderes,Ecchi" grado="Z" personaje="Lin Ying Jie (zero)"/>
														<SerieItem img="dpcq" title="Dou Po Cang Qiong" autor="Heavenly Silkworm Potato" estado="En emisión" type="Manhua,Donghua" 
															tags="Acción,Cultivación,Aventuras,Artes marciales,Fantasia,Romance" grado="SS" personaje="Xiao Yan"/>
														<SerieItem img="iswakka" title="It Starts With a Kingpin Account" autor="Renshan Animation" estado="En emisión" type="Manhua" 
															tags="Acción,Artes marciales,Harem,Supervivencia,Apocalíptico,Tragedia" grado="S" personaje="Ye Hao"/>
														<SerieItem img="magicE" title="Magic Emperor" autor="Nightngale" estado="En emisión" type="Manhua" 
															tags="Acción,Cultivación,Aventuras,Artes marciales,Fantasia,Romance" grado="S" personaje="Zhuo Fan"/>
													</Row>
												</Col>
											</Row>
										</Container>
									</Tab.Pane>
									<Tab.Pane eventKey="fifth">
										{/* Tab Fifth */}
										<Container className='' >
											{/* Tabs Musica */}
											<Row className="">
												<Col className='d-flex justify-content-center align-items-center my-3'>
													<h3>Canciones favoritas</h3>
												</Col>
											</Row>
										</Container>
										<Container style={{overflowY: "scroll", height: "545px"}}>
											<Row className='py-3' style={{backgroundColor: "#0008", border: "5px solid #FFD700"}}>
												<SongItem img="s-emeraldSword.jpg" title="Emerald Sword" autor="Rhapsody of Fire" tags="Power metal,Heavy metal,Metal progresivo" />
												<SongItem img="s-ThunderMightyRoar.jpg" title="Thunder's Mighty Roar" autor="Rhapsody of Fire" tags="Heavy metal" />
												<SongItem img="s-landOfInmortals.jpg" title="Land of inmortals" autor="Rhapsody of Fire" tags="Heavy metal" />
												<SongItem img="s-IWillBeYourHero.jpg" title="I'll Be Your Hero" autor="Rhapsody of Fire" tags="Power metal" />
												<SongItem img="s-defendersOfGaia.jpg" title="Defenders of Gaia" autor="Rhapsody of Fire" tags="Heavy metal" />
												<SongItem img="s-TheEmperor.jpg" title="The Emperor" autor="Dark Moor" tags="Heavy metal" />
												<SongItem img="s-MyEternalDream.jpg" title="My Eternal Dream" autor="Stratovarius" tags="Heavy metal, Rock" />
												<SongItem img="s-survive.jpg" title="Survive" autor="Stratovarius" tags="Heavy metal" />
												<SongItem img="s-buryTheLight.jpg" title="Bury the Light" autor="Casey Edwards" tags="Heavy metal,Halloween music,Video Game" />
												<SongItem img="s-devilTrigger.jpg" title="Devil Trigger" autor="Casey Edwards" tags="Heavy metal,Halloween music,Video Game" />
												<SongItem img="s-diesIrae.jpg" title="Dies Irae" autor="Beyond the Black" tags="Heavy metal" />
												<SongItem img="s-theLandOfUnicorns.jpg" title="The Land of Unicorns" autor="Gloryhammer" tags="Heavy metal" />
											</Row>
										</Container>
									</Tab.Pane>
								</Tab.Content>
							</Col>
						</Row>
					</Tab.Container>
				</Col>
			</Row>
		</Fragment>
	);
}

export default Profile;
