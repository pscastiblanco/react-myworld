import React, {Fragment}  from 'react';

// css
import '../../css/museum.css'
import {Row, Col, Modal,Button, Container} from 'react-bootstrap';

export function Museum(){
	var pathMuseo = "img/jpg/museo/";
	
	const [modalShow, setModalShow] = React.useState(false);
	const [cacheDataGallery, setCacheData] = React.useState();
	
	const addDataIntoCacheMuseo = (cacheName, url, response) => {
		// Converting our response into Actual Response form
		const data = new Response(JSON.stringify(response));
		if ('caches' in window) {
			// Opening given cache and putting our data into it
			caches.open(cacheName).then((cache) => {
				cache.put(url, data);
				//alert('Data Added into cache!')
			});
		}
	};
	
	const getAllCacheDataMuseo = async () => {
		var url = 'https://localhost:300'
		// List of all caches present in browser
		//var names = await caches.keys();
		var name ="ModalGaleryImg";
		var cacheDataArray = []
		// Iterating over the list of caches
		//names.forEach(async(name) => {
			// Opening that particular cache
			const cacheStorage = await caches.open(name);
			// Fetching that particular cache data
			const cachedResponse = await cacheStorage.match(url);
			var data = await cachedResponse.json()
			// Pushing fetched data into our cacheDataArray
			cacheDataArray.push(data)
			setCacheData(cacheDataArray.join(', '))
		//})
	}

	
	const setGallerySeleted = (img) => {
		addDataIntoCacheMuseo('ModalGaleryImg','https://localhost:300',img);
		setModalShow(true);
	};
	
	function ModalGalery(props){
		getAllCacheDataMuseo();
		var idImg = 0;
		const subImgItems = [ {"descripcion":null,"subImagenes":[]}
			,{"descripcion":"Mural de imagenes 1"
			,"subImagenes":[]
			}
			,{"descripcion":"Expositor de hobbies"
			,"subImagenes":["Sección de figuras grandes","Sección de Nendoroids","Sección de figuras pequeñas","Sección de hobbies varios"]
			}
			,{"descripcion":"Dibujo de Ib y Gary. Juego: Ib"
			,"subImagenes":[]
			}
			,{"descripcion":"Rincon de la nostalgia"
			,"subImagenes":["El tren del abuelo","Recuerds de viajes","Figura: Dragoniod Colosus","Pintura","Lampara Tetris"]
			}
			,{"descripcion":"Regalos y una meta"
			,"subImagenes":["Regalos de Paula, Jorge y Paula-Hansel respectivamente","Eymith"]
			}
			,{"descripcion":"Poster de Hatsune Miku."
			,"subImagenes":[]
			}
			,{"descripcion":"Rincon del gamer"
			,"subImagenes":["Juegos y hama beans","Mangas","Consolas","Ajedres y cubos"]
			}
			,{"descripcion":"Regalo de cumpleaños de Josephine. Anime: Goblin slayer"
			,"subImagenes":[]
			}
			,{"descripcion":"El PC"
			,"subImagenes":["Figura anime: Sagiri Izumi - Eromanga-sensei","Figura anime: Set de figuras de Demon Slayer"]
			}
			,{"descripcion":"Mural de dibujos 2"
			,"subImagenes":[]
			}
			,{"descripcion":"Mural de dibujos 3"
			,"subImagenes":[]
			}
			,{"descripcion":"Lluvia de recuerdos."
			,"subImagenes":[]
			}
		];

		const setMiniImg = (subImgItems,img,idImg,subImg) =>{
			if(subImg != null){
				document.getElementById('imgModalMini').src = pathMuseo + img.substring(0, 4) + "-" + subImg + ".jpeg";
				document.getElementById('descripcionModal').innerHTML = subImgItems[idImg].subImagenes[subImg];
				return;
			}
			document.getElementById('imgModalMini').src = pathMuseo + img;
			document.getElementById('descripcionModal').innerHTML = subImgItems[idImg].descripcion;
		}

		console.log("galeria " + cacheDataGallery);
		if (cacheDataGallery != undefined){
			var idImg = +cacheDataGallery.substring(2, 4);
			console.log("id " + idImg);
		}
		
		return(
			<Modal {...props} aria-labelledby="contained-modal-title-vcenter" centered dialogClassName="my-modal">
				<Modal.Header closeButton >
					<Modal.Title id="contained-modal-title-vcenter">
						Dibujo ampliado
					</Modal.Title>
				</Modal.Header>
				<Modal.Body className='d-flex justify-content-center '>
					<Container fluid>
						<Row>
							<Col sm={6} className='d-flex justify-content-center align-items-center'>
								<img id={"imgModal"} src={pathMuseo + cacheDataGallery} className={""} alt="item" style={{maxHeight: "400px" , width: "auto", maxWidth: "100%"}} />
							</Col>
							<Col sm={2} className="bg-dark">
								{( () => {
									const listdiv = [];
									for (let i = 0; i < subImgItems[idImg].subImagenes.length; i++){
										listdiv.push(
											<Row key={i} className="my-1" style={{height: "70px", overflow: "hidden"}}>
												<img id={"imgModalItem"} src={pathMuseo + cacheDataGallery.substring(0, 4) + "-" + i + ".jpeg"} className={"imgModalItem"} alt="item" style={{maxHeight: "auto" , width: "100%", maxWidth: "100%", cursor: "pointer"}} 
												key={i} onClick={() => setMiniImg(subImgItems,cacheDataGallery,idImg,i)}
												/>
											</Row>
										);
									}
									return listdiv;
								})()}
							</Col>
							<Col sm={4}>
								<Row>
									<Col sm={12} className='d-flex justify-content-center align-items-center'>
										<img id={"imgModalMini"} src={pathMuseo + cacheDataGallery} className={""} alt="item" style={{maxHeight: "200px" , width: "auto", maxWidth: "100%"}} 
										/>
									</Col>
									<Col sm={12} className="py-3">
										<h5>Descripción:</h5>
										<p id='descripcionModal'>{subImgItems[idImg].descripcion}</p>
									</Col>
								</Row>
							</Col>
						</Row>
					</Container>
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={props.onHide}>Cerrar</Button>
				</Modal.Footer>
			</Modal>
		);
	}
	
	return (
		<Fragment>
			<Row className='gallery mt-3 mb-5'>
				<Col sm={12} className='d-flex justify-content-center align-items-center'>
					<h1 className='tituloMuseo my-5'>My Museo</h1>
				</Col>
				<Col className='grid my-4' style={{height: "450px"}}>
					<div className='item img01' onClick={() => setGallerySeleted("m-01.jpeg")}><img src={pathMuseo + "m-01.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img02' onClick={() => setGallerySeleted("m-06.jpeg")}><img src={pathMuseo + "m-06.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img03' onClick={() => setGallerySeleted("m-02.jpeg")}><img src={pathMuseo + "m-02.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img04' onClick={() => setGallerySeleted("m-03.jpeg")}><img src={pathMuseo + "m-03.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img05' onClick={() => setGallerySeleted("m-04.jpeg")}><img src={pathMuseo + "m-04.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img06' onClick={() => setGallerySeleted("m-05.jpeg")}><img src={pathMuseo + "m-05.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img07' onClick={() => setGallerySeleted("m-07.jpeg")}><img src={pathMuseo + "m-07.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img08' onClick={() => setGallerySeleted("m-08.jpeg")}><img src={pathMuseo + "m-08.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img09' onClick={() => setGallerySeleted("m-09.jpeg")}><img src={pathMuseo + "m-09.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img10' onClick={() => setGallerySeleted("m-10.jpeg")}><img src={pathMuseo + "m-10.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img11' onClick={() => setGallerySeleted("m-11.jpeg")}><img src={pathMuseo + "m-11.jpeg"} className="" alt="item" style={{}} /></div>
					<div className='item img12' onClick={() => setGallerySeleted("m-12.jpeg")}><img src={pathMuseo + "m-12.jpeg"} className="" alt="item" style={{}} /></div>
				</Col>
				<ModalGalery show={modalShow} onHide={() => setModalShow(false)} />
			</Row>
		</Fragment>
	);
}