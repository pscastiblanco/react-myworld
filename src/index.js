import React from 'react';
//import ReactDOM from 'react-dom';
import {createRoot} from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";
// Bootstrap Bundle JS
//import "bootstrap/dist/js/bootstrap.bundle.min";
//import "bootstrap/dist/js/bootstrap.min"

// Components
import {App} from "./App";
const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
		<BrowserRouter>
			<App />
		</BrowserRouter>
		, document.getElementById('root')
	);
